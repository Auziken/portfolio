$( document ).ready( function() {

  var blindmode = false;
  var currentpage = 1;

  function speakText(text) {
        // Create a new SpeechSynthesisUtterance object with the text to speak
        var utterance = new SpeechSynthesisUtterance(text);
        
        // Speak the text
        window.speechSynthesis.speak(utterance);
  }

  $(document).keydown(function(e) {
    if (e.keyCode === 38) { // enable "blindness mode"
        blindmode = true;
        speakText("Blind navigation mode enabled. You can use the left and right arrow keys to switch pages. When entering a new page, a brief description will be given. You can press F to hear more about that page. If you want to stop all ongoing speech, you can do so by pressing the J key. Finally, if you would like to hear these directions again at any time, you can press the up arrow again.")
    }
    if (e.keyCode === 74){ // stop all sound
      window.speechSynthesis.cancel();
    }

    if (e.keyCode === 70){
      switch (currentpage){
        case 1:
          window.speechSynthesis.cancel();
          speakText("Hello! My name is Samuel Buxton, and I am currently an undergrad student at the University of Missouri—Kansas City. I am pursuing a bachelor's in computer science and have earned multiple computer science certificates during my educational career. My degree's focus is mostly software development and creating applications for clients to use. I have done a few personal projects on the side, mainly applications to work in tandem with video games, but a few I have made for friends or family for specific purposes. I am always trying to learn as much as possible and figuring out new ways to do what I do even better!")
          break;
        case 2:
          window.speechSynthesis.cancel();
          speakText("Resume text here.")
          break;
        case 3:
          window.speechSynthesis.cancel();
          speakText("I graduated from Blue Springs South High School after already taking a few computer science courses, as well as Calculus I and a few other college-level courses. The introductory courses offered at my high school were the stepping stones to my interest in this field. After graduating high school in 2021 with a few college-level classes under my belt, I started at Metropolitan Community College to take advantage of my A+ Scholarship and began to take my general education classes, along with a few computer science courses, like Object-Oriented C++. Instead of staying the full 2(?) years and acquiring my Associate's like I had planned, an assistant Dean at UMKC suggested me to enroll a semester early. I proceeded to do so and I moved to the University of Missouri—Kansas City to continue to take my Computer Science courses, which is where I am as of 2023.")
          break;
        case 4:
          window.speechSynthesis.cancel();
          speakText("I volunteered at the A+ program offered at my high school - Blue Springs South - between 2019 and 2020, where I volunteered as a teacher aid. As for community service, I have been volunteering at the No Kid Goes Hungry program every summer since 2016 where we go out to neighborhoods and give people school lunches during the summer where they would otherwise not get any. Finally, I have interned at Cerner between January and June 2021 through a program that was offered through Fort Osage's Career and Technology Center.")
          break;
        case 5:
          window.speechSynthesis.cancel();
          speakText("You can contact me at sammy dot buxton i x @ gmail dot com, or at 816-838-3034.")
      }
    }


    if (e.keyCode === 37){ // last page
      switch (currentpage){
        case 1: // welcome -> contact
          currentpage = 5;
          $("#WIN_WEL").hide();
          $("#WIN_CV").hide();
          $("#WIN_BKG").hide();
          $("#WIN_SER").hide();
          $("#WIN_CON").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the contact information page. This page has Samuel Buxton's contact information. Press F to hear more.");
            }
          });
          break;
        case 2: // resume -> welcome
          currentpage = 1;
          $("#WIN_CON").hide();
          $("#WIN_CV").hide();
          $("#WIN_BKG").hide();
          $("#WIN_SER").hide();
          $("#WIN_WEL").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the welcome page. This page has information about Samuel Buxton. Press F to hear more.");
            }
          });
          break;
        case 3: // education -> resume
          currentpage = 2;
          $("#WIN_WEL").hide();
          $("#WIN_CON").hide();
          $("#WIN_BKG").hide();
          $("#WIN_SER").hide();
          $("#WIN_CV").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the resume page. This page contains Samuel Buxton's entire resume. Press F to begin reading it aloud.");  
            }
          });
          break;
        case 4: // service -> education
          currentpage = 3;
          $("#WIN_WEL").hide();
          $("#WIN_CV").hide();
          $("#WIN_CON").hide();
          $("#WIN_SER").hide();
          $("#WIN_BKG").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the education background page. This page has all of the previous education Samuel Buxton has taken. Press F to hear more.");
            }
          });
          break;
        case 5: // contact -> service
          currentpage = 4;
          $("#WIN_WEL").hide();
          $("#WIN_CV").hide();
          $("#WIN_BKG").hide();
          $("#WIN_CON").hide();
          $("#WIN_SER").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the service and work page. This page has information about previous services and jobs Samuel Buxton has had. Press F to hear more.");
            }
          });
          break;
      }
    }

    
    if (e.keyCode === 39){ // next page
      switch (currentpage){
        case 1: // welcome -> resume
          currentpage = 2;
          $("#WIN_WEL").hide();
          $("#WIN_CON").hide();
          $("#WIN_BKG").hide();
          $("#WIN_SER").hide();
          $("#WIN_CV").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the resume page. This page contains Samuel Buxton's entire resume. Press F to begin reading it aloud.");  
            }
          });
          break;
        case 2: // resume -> education
          currentpage = 3;
          $("#WIN_WEL").hide();
          $("#WIN_CV").hide();
          $("#WIN_CON").hide();
          $("#WIN_SER").hide();
          $("#WIN_BKG").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the education background page. This page has all of the previous education Samuel Buxton has taken. Press F to hear more.");
            }
          });
          break;
        case 3: // education -> service
          currentpage = 4;
          $("#WIN_WEL").hide();
          $("#WIN_CV").hide();
          $("#WIN_BKG").hide();
          $("#WIN_CON").hide();
          $("#WIN_SER").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the service and work page. This page has information about previous services and jobs Samuel Buxton has had. Press F to hear more.");
            }
          });
          break;
        case 4: // service -> contact
          currentpage = 5;
          $("#WIN_WEL").hide();
          $("#WIN_CV").hide();
          $("#WIN_BKG").hide();
          $("#WIN_SER").hide();
          $("#WIN_CON").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the contact information page. This page has Samuel Buxton's contact information. Press F to hear more.");
            }
          });
          break;
        case 5: // contact -> welcome
          currentpage = 1;
          $("#WIN_CON").hide();
          $("#WIN_CV").hide();
          $("#WIN_BKG").hide();
          $("#WIN_SER").hide();
          $("#WIN_WEL").fadeIn("fast", function() {
            if (blindmode){
              window.speechSynthesis.cancel();
              speakText("You are on the welcome page. This page has information about Samuel Buxton. Press F to hear more.");
            }
          });
          break;
      }
    }
  });

    $("#BTN_WEL" ).click( function() { $("#WIN_WEL").fadeIn("fast", function() {}); });
    $("#BTN_WEL").click(function(){ $("#WIN_TEA").hide();});
    $("#BTN_WEL").click(function(){ $("#WIN_SCH").hide();});
    $("#BTN_WEL").click(function(){ $("#WIN_SER").hide();});
    $("#BTN_WEL").click(function(){ $("#WIN_CON").hide();});
    $("#BTN_WEL").click(function(){ $("#WIN_CV").hide();});
    $("#BTN_WEL").click(function(){ $("#WIN_BKG").hide();});
    $("#BTN_WEL").click(function(){ $("#WIN_DIV").hide();});
    $("#BTN_WEL").click(function(){ $("#WIN_SAM").hide();});
    $("#BTN_WEL").click(function(){ $("#WIN_PHIL").hide();});
    $("#BTN_WEL").click(function(){ $("#WIN_CON").hide();});

    $("#BTN_SER" ).click( function() {$("#WIN_SER").fadeIn("fast", function() {});});
    $("#BTN_SER").click(function(){ $("#WIN_WEL").hide();});
    $("#BTN_SER").click(function(){ $("#WIN_TEA").hide();});
    $("#BTN_SER").click(function(){ $("#WIN_SCH").hide();});
    $("#BTN_SER").click(function(){ $("#WIN_CON").hide();});
    $("#BTN_SER").click(function(){ $("#WIN_CV").hide();});
    $("#BTN_SER").click(function(){ $("#WIN_BKG").hide();});
    $("#BTN_SER").click(function(){ $("#WIN_DIV").hide();});
    $("#BTN_SER").click(function(){ $("#WIN_SAM").hide();});
    $("#BTN_SER").click(function(){ $("#WIN_PHIL").hide();});
    $("#BTN_SER").click(function(){ $("#WIN_CON").hide();});

    $("#BTN_CON" ).click( function() {
    $("#WIN_CON").fadeIn("fast", function() {});});
    $("#BTN_CON").click(function(){ $("#WIN_WEL").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_TEA").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_SCH").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_SER").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_CV").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_BKG").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_DIV").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_SAM").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_PHIL").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_CON").hide();});

    $( "#BTN_CV" ).click( function() {$("#WIN_CV").fadeIn("fast", function() {});});
    $("#BTN_CV").click(function(){ $("#WIN_WEL").hide();});
    $("#BTN_CV").click(function(){ $("#WIN_TEA").hide();});
    $("#BTN_CV").click(function(){ $("#WIN_SCH").hide();});
    $("#BTN_CV").click(function(){ $("#WIN_SER").hide();});
    $("#BTN_CV").click(function(){ $("#WIN_CON").hide();});
    $("#BTN_CV").click(function(){ $("#WIN_BKG").hide();});
    $("#BTN_CV").click(function(){ $("#WIN_DIV").hide();});
    $("#BTN_CV").click(function(){ $("#WIN_SAM").hide();});
    $("#BTN_CV").click(function(){ $("#WIN_PHIL").hide();});
    $("#BTN_CV").click(function(){ $("#WIN_CON").hide();});

    $( "#BTN_BKG" ).click( function() {$("#WIN_BKG").fadeIn("fast", function() {});});
    $("#BTN_BKG").click(function(){ $("#WIN_WEL").hide();});
    $("#BTN_BKG").click(function(){ $("#WIN_TEA").hide();});
    $("#BTN_BKG").click(function(){ $("#WIN_SCH").hide();});
    $("#BTN_BKG").click(function(){ $("#WIN_SER").hide();});
    $("#BTN_BKG").click(function(){ $("#WIN_CON").hide();});
    $("#BTN_BKG").click(function(){ $("#WIN_CV").hide();});
    $("#BTN_BKG").click(function(){ $("#WIN_DIV").hide();});
    $("#BTN_BKG").click(function(){ $("#WIN_SAM").hide();});
    $("#BTN_BKG").click(function(){ $("#WIN_PHIL").hide();});
    $("#BTN_BKG").click(function(){ $("#WIN_CON").hide();});

    // unsure why theres a duplicate set here but the page doesnt show without it.
    $("#BTN_CON" ).click( function() {$("#WIN_CON").fadeIn( "fast", function(){} );});
    $("#BTN_CON").click(function(){ $("#WIN_WEL").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_TEA").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_SCH").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_SER").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_CON").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_CV").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_BKG").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_DIV").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_SAM").hide();});
    $("#BTN_CON").click(function(){ $("#WIN_PHIL").hide();});

    $( ".BTN_CLOSE" ).click( function() {
      $( this ).closest( ".window" ).fadeOut( "fast", function() {} );
    } );
  } );